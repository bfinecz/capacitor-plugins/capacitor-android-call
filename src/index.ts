import { registerPlugin } from '@capacitor/core';

import type { AndroidCallPlugin } from './definitions';

const AndroidCall = registerPlugin<AndroidCallPlugin>('AndroidCall', {
  web: () => import('./web').then(m => new m.AndroidCallWeb()),
});

export * from './definitions';
export { AndroidCall };
