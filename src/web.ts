import { WebPlugin } from '@capacitor/core';

import type { AndroidCallPlugin } from './definitions';

export class AndroidCallWeb extends WebPlugin implements AndroidCallPlugin {
  async echo(options: { value: string }): Promise<{ value: string }> {
    console.log('ECHO', options);
    return options;
  }
}
