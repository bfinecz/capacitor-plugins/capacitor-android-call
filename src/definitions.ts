export interface AndroidCallPlugin {
  echo(options: { value: string }): Promise<{ value: string }>;
}
