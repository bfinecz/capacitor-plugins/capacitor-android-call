import Foundation

@objc public class AndroidCall: NSObject {
    @objc public func echo(_ value: String) -> String {
        return value
    }
}
