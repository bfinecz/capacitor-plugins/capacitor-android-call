package com.bfine.capacitor.androidcall;

import android.Manifest;
import android.content.ComponentName;
import android.os.Build;
import android.os.Bundle;

import android.telecom.PhoneAccount;
import android.telecom.TelecomManager;
import android.telecom.PhoneAccountHandle;

import com.getcapacitor.PermissionState;
import com.getcapacitor.Plugin;
import com.getcapacitor.PluginCall;
import com.getcapacitor.PluginMethod;
import com.getcapacitor.annotation.CapacitorPlugin;
import com.getcapacitor.annotation.Permission;
import com.getcapacitor.annotation.PermissionCallback;
import android.content.Context;

import androidx.annotation.RequiresApi;

@RequiresApi(api = Build.VERSION_CODES.O)
@CapacitorPlugin(
        name = "AndroidCall",
        permissions = {
        @Permission(
                alias = "call",
                strings = {
                        Manifest.permission.CALL_PHONE,
                        Manifest.permission.MANAGE_OWN_CALLS,
                        Manifest.permission.BIND_TELECOM_CONNECTION_SERVICE
                }
        )
        }
)
public class AndroidCallPlugin extends Plugin {

    private AndroidCall implementation = new AndroidCall();
    private TelecomManager tm;
    private int permissionCounter = 0;
    private PhoneAccountHandle handle;
    private PhoneAccount phoneAccount;

    @Override
    public void load(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            handle = new PhoneAccountHandle(new ComponentName(this.getActivity().getApplicationContext(),MyConnectionService.class),"BetterCall");
        }
        this.getActivity().getApplicationContext();
        tm = (TelecomManager)this.getActivity().getApplicationContext().getSystemService(Context.TELECOM_SERVICE);
        if(android.os.Build.VERSION.SDK_INT >= 26) {
            phoneAccount = new PhoneAccount.Builder(handle, "BetterCall")
                    .setCapabilities(PhoneAccount.CAPABILITY_SELF_MANAGED)
                    .build();
            tm.registerPhoneAccount(phoneAccount);
        }
        if(android.os.Build.VERSION.SDK_INT >= 23) {
            phoneAccount = new PhoneAccount.Builder(handle, "BetterCall")
                    .setCapabilities(PhoneAccount.CAPABILITY_CALL_PROVIDER)
                    .build();
            tm.registerPhoneAccount(phoneAccount);
        }
    }

    public void arrangePermissions(PluginCall call){
        if (getPermissionState("call") != PermissionState.GRANTED) {
            requestPermissionForAlias("call", call, "callPermsCallback");
        }
    }

    @PermissionCallback
    private void callPermsCallback(PluginCall call) {
        if (getPermissionState("call") == PermissionState.GRANTED) {
            if (call != null) call.resolve();
        } else {
            if (call != null) call.reject("Permission is required to init calls");
        }
    }

    @PluginMethod
    public void echo(PluginCall call) {
        arrangePermissions(call);
        Bundle callInfo = new Bundle();
        callInfo.putString("from","Yurii Leso");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            tm.addNewIncomingCall(handle, callInfo);
        }
        permissionCounter = 0;
        call.resolve();
    }

}
